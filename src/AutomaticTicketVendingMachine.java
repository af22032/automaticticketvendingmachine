import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AutomaticTicketVendingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton sobaButton;
    private JButton katsudonButton;
    private JButton yakisobaButton;
    private JTextArea orderedFoods;
    private JButton checkOut;
    private JLabel OrderedFoods;
    private JLabel TotalPrice;
    private JButton BigSize;
    private JButton SmallSize;
    private JButton MegaSize;
    private JLabel menu;
    private JLabel size;
    private JRadioButton radioButton1;
    private JButton cancel;
    int Sum = 0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("AutomaticTicketVendingMachine");
        frame.setContentPane(new AutomaticTicketVendingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /*** Sizeの選択不可 ***/
    void order1(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            String currentTotalOrder = orderedFoods.getText();
            orderedFoods.setText(currentTotalOrder + food + " " + price + "yen" + "\n");
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + ".");
            Sum += price;
        }
        else if(confirmation == 1){
            JOptionPane.showMessageDialog(null, "You Cancel " + food + ".");
        }
        TotalPrice.setText("Total Price: " + Sum + "yen" + "\n");
    }

    /*** Sizeの選択可 ***/
    void order2(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            String currentTotalOrder = orderedFoods.getText();
            orderedFoods.setText(currentTotalOrder + food + " " + price + "yen" + "\n");
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + ".");
            Sum += price;

        }
        else if(confirmation == 1){
            JOptionPane.showMessageDialog(null, "You Cancel " + food + ".");
        }
        TotalPrice.setText("Total Price: " + Sum + "yen" + "\n");
    }

    /*** 食品ごとにサイズ選択できるか否かorderで分ける(TempuraのBigが選択可能になってしまう)
     * drinkやriceをセットでつけられるようにするのも良い!***/

    void size(String size, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + size + "Size?",
                "Size Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            String currentTotalOrder = orderedFoods.getText();
            orderedFoods.setText(currentTotalOrder + "(" + size + " " + price + "yen)" + "\n");
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + size + " Size.");
            Sum += price;
        }
        else if(confirmation == 1){
            JOptionPane.showMessageDialog(null, "You Cancel.");
        }
        TotalPrice.setText("Total Price: " + Sum + "yen" + "\n");
    }

    void checkOut(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "Thank you. The total price is " + Sum + "yen.");
            Sum = 0;
            orderedFoods.setText("");
            TotalPrice.setText("Total Price: 0yen");
        }
        else if(confirmation == 1){
            JOptionPane.showMessageDialog(null, "Please continue to order.");
        }
    }

    void cancel(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to cancel?",
                "Cancel Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "Thank you. The total price is " + Sum + "yen.");
            Sum = 0;
            orderedFoods.setText("");
            TotalPrice.setText("Total Price: 0yen");
        }
        else if(confirmation == 1){
            JOptionPane.showMessageDialog(null, "Please continue to order.");
        }
    }

    public AutomaticTicketVendingMachine() {
        tempuraButton.setIcon(new ImageIcon("C:\\Users\\yuta1\\Downloads\\adpDSC_5435_1.jpg"));
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order1("Tempura", 600);
            }
        });
        ramenButton.setIcon(new ImageIcon("C:\\Users\\yuta1\\Downloads\\pexels-ngqah83-884600_1.jpg"));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Ramen", 500);
            }
        });
        udonButton.setIcon(new ImageIcon("C:\\Users\\yuta1\\Downloads\\1000002873_1.jpg"));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Udon", 400);
            }
        });
        sobaButton.setIcon(new ImageIcon("C:\\Users\\yuta1\\Downloads\\29195086_s_1.jpg"));
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Soba", 1000);
            }
        });
        katsudonButton.setIcon(new ImageIcon("C:\\Users\\yuta1\\Downloads\\26832931_s_1.jpg"));
        katsudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Katsudon", 450);
            }
        });
        yakisobaButton.setIcon(new ImageIcon("C:\\Users\\yuta1\\Downloads\\pexels-bishop-tamrakar-2246201-3926135_1.jpg"));
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order2("Yakisoba", 300);
            }
        });

        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkOut();
            }
        });
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancel();
            }
        });

        SmallSize.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                size("Small", -50);
            }
        });
        BigSize.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                size("Big", +100);
            }
        });
        MegaSize.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                size("Mega", +200);
            }
        });
        radioButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                size("Good", 600000);
            }
        });

    }
}

/*** 出来なかったことは考察に書く!
 * 卒研とかでも一緒! ***/